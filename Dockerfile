FROM python:3.9.1-slim-buster

WORKDIR "/app"
COPY . .
ENV PORT="80"
RUN pip install -r requirements.txt
CMD ["python3", "manage.py", "run"]