from marshmallow import Schema, fields

boxSchema = Schema.from_dict({
    "x": fields.Int(required=True),
    "y": fields.Int(required=True),
    "width": fields.Int(required=True),
    "height": fields.Int(required=True)
})

textSchema = Schema.from_dict({
        "content": fields.String(required=True),
        "text_color": fields.String(required=True),
        "border_color": fields.String(required=True)
})

drawSchema = Schema.from_dict({
    "font_url": fields.String(required=True),
    "image_url": fields.URL(required=True),
    "text" : fields.Nested(textSchema, required=True),
    "box": fields.Nested(boxSchema, required=True)
})