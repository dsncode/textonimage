import pytest
from app.services.text import TextService, TextDoesNotFitBoxException
from app.model.payload import Box, Split

@pytest.fixture
def service():
    return TextService()


def test_fit_words_on_box(service):
    # expectations
    split1 = Split(content="Dipp inc, thinking", font_size=59, x=40, y=100)
    split2 = Split(content="out of how to draw", font_size=59, x=40, y=159)
    split3 = Split(content="a text on the box.", font_size=59, x=40, y=218)
    expected_splits = [split1, split2, split3]

    # test data
    content = "Dipp inc, thinking out of how to draw a text on the box."
    box = Box(height=180, width=500, x=40, y=100)
    font_location = "tests/storage/samples/font.ttf"
    best_splits = service.find_best_combination(content, font_location, box)

    assert len(expected_splits) == len(best_splits)

    for index in range(len(expected_splits)):
        assert expected_splits[index].content == best_splits[index].content
        assert expected_splits[index].font_size == best_splits[index].font_size
        assert expected_splits[index].x == best_splits[index].x
        assert expected_splits[index].y == best_splits[index].y

def test_cant_fit_words_on_box(service):
    # test data
    content = "Dipp inc, thinking out of how to draw a text on the box."
    # too small box
    box = Box(height=1, width=1, x=100, y=100)
    font_location = "tests/storage/samples/font.ttf"
    with pytest.raises(TextDoesNotFitBoxException):
        service.find_best_combination(content, font_location, box)