import pytest
from app.model.payload import Split, Box
from app.services.images import ImagesService

@pytest.fixture
def service():
    return ImagesService("tests/storage")


def test_image_generation(service):
    source_image_path = "tests/storage/samples/image.jpg"
    font_path = "tests/storage/samples/font.ttf"
    target_image_name = "output.jpg"
    box = Box(x=40, y=100, width=500, height=180)
    font_size = 59
    split1 = Split(content="Dipp inc, thinking", font_size=font_size, x=40, y=100)
    split2 = Split(content="out of how to draw", font_size=font_size, x=40, y=159)
    split3 = Split(content="a text on the box.", font_size=font_size, x=40, y=218)
    splits = [split1, split2, split3]
    response = service.patch_image(image_path=source_image_path, target_image_name=target_image_name, font_path=font_path, box=box, splits=splits)

