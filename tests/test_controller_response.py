import pytest
from flask import json
from app.app import create_app


@pytest.fixture
def client():
    app = create_app()
    with app.test_client() as client:
        yield client


def test_not_found_url(client):
    response = client.get("/api/v1/hello")
    json_response = json.loads(response.data)
    assert json_response['message'] == "Nothing matches the given URI"
    assert response.status_code == 404


def test_method_does_not_exist(client):
    response = client.put("/api/v1/draw")
    json_response = json.loads(response.data)
    assert json_response['message'] == "Specified method is invalid for this resource"
    assert response.status_code == 405


def test_bad_request(client):
    # No Content
    response = client.post("/api/v1/draw") # no content
    json_response = json.loads(response.data)
    assert json_response['message'] == "Please inspect your params or query valid"
    assert response.status_code == 400

    # missing field (text.content)
    payload = {
       "font_url": "https://storage.googleapis.com/dipp-massimo-development-fonts/4f2cf2b6b99d96ca.ttf",
        "image_url": "https://storage.googleapis.com/dipp-massimo-development-images/1f1282fef735f349.jpg",
        "text": {
            "text_color": "#000000",
            "border_color": "#000000"
        },
        "box": {
            "x": 40,
            "y": 50,
            "width": 500,
            "height": 180
        }
    }
    response = client.post("/api/v1/draw", data=payload)  # no content
    json_response = json.loads(response.data)
    assert json_response['message'] == "Please inspect your params or query valid"
    assert response.status_code == 400




