# Fit Text into a box Application v 0.1.0

## 1 Introduction
This application, will help to fit a text within a box into an image.

## 2 How To Run it

### 2.1 Via Python on local environment

- please make sure you have python 3.8 installed
- create a virtual environment
```bash
virtualenv -p python3 <name of virtualenv>
```
- install all dependencies
```bash
python3 pip -r requireemnts.txt
```

start the application
```bash
bash start.sh
```

run all unit tests
```bash
pytest
```

### 2.2 Via Docker

- please pull this image from dockerhub
```bash
docker pull dsncode/dipp
```
- update docker-compose variables accordingly
```bash
#Hostname where you want to host this application
HOST_NAME=dipp.dsncode.com 
# Scheme where this application will be accessed from (http or https)
SCHEME=https
```
- run it locally using docker compose
```bash
docker-compose -f docker-compose.yml up -d
```

## 3. Available API

### GET          /api/v1/
health check endpoint

### POST         /api/v1/draw
api to draw a text, within a box
payload should be as follows
```json
{
    "font_url": "https://storage.googleapis.com/dipp-massimo-development-fonts/4f2cf2b6b99d96ca.ttf",
    "image_url": "https://i.pinimg.com/originals/a4/f8/f9/a4f8f91b31d2c63a015ed34ae8c13bbd.jpg",
    "text": {
        "content": "Some cool text into a small box - Daniel",
        "text_color": "#ffffff",
        "border_color": "#ffffff"
    },
    "box": {
        "x": 260,
        "y": 650,
        "width": 400,
        "height": 180
    }
}
```

which will return a response like this

```json
{
    "box": {
        "height": 180,
        "width": 400,
        "x": 260,
        "y": 650
    },
    "resource": "/api/v1/images/97bd2a8c-8570-4a84-a126-239ff8316d2b.jpg",
    "splits": [
        {
            "content": "Some cool text",
            "font_size": 59,
            "x": 260,
            "y": 650
        },
        {
            "content": "into a small",
            "font_size": 59,
            "x": 260,
            "y": 709
        },
        {
            "content": "box - Daniel",
            "font_size": 59,
            "x": 260,
            "y": 768
        }
    ]
}
```

and it will look like this
![image](./images/demo.jpg)

### GET          /api/v1/images/<filename>
here. please set the filename id to retrieve the image
