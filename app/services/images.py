import uuid
from PIL import Image, ImageFont, ImageDraw, ImageColor
from app.model.payload import Text
import urllib.request
import os


class ImagesService:

    def __init__(self, target_folder="storage", default_box_outline="#000000", default_text_border_size=1):
        self.target_folder = target_folder
        self.target_folder_image = f'{self.target_folder}/images'
        self.target_folder_fonts = f'{self.target_folder}/fonts'
        self.default_box_outline = default_box_outline
        self.default_text_border_size = default_text_border_size

    def patch_image(self, image_path, target_image_name=None, font_path="", box=None, splits=[], text=Text()):

        # 1. create image
        image_ref = Image.open(image_path)

        # 2. patch image
        image_editable = ImageDraw.Draw(image_ref)

        # 2.1 draw container box
        text_color = text.text_color
        border_color = text.border_color
        start_location = (box.x, box.y)
        shape = [start_location, (box.x + box.width, box.y + box.height)]
        image_editable.rectangle(shape, outline=self.default_box_outline)

        # 2.2 draw text
        for split in splits:
            text_font_color = ImageColor.getrgb(text_color)
            text_font_border_color = ImageColor.getrgb(border_color)
            text_font = ImageFont.truetype(font_path, split.font_size)
            start_location = (split.x, split.y)
            image_editable.text(xy=start_location, text=split.content, fill=text_font_color, font=text_font, stroke_fill=text_font_border_color, stroke_width=self.default_text_border_size)

        if target_image_name is None:
            target_image_name = f'{uuid.uuid4()}.jpg'

        resource = f'{self.target_folder_image}/{target_image_name}'
        image_ref.save(resource)

        return target_image_name

    def find_image_location(self, image_id):
        return f'{self.target_folder_image}/{image_id}'

    def download_image(self, source):
        return self.__download(self.target_folder_image, source)

    def download_font(self, source):
        return self.__download(self.target_folder_fonts, source)

    def __download(self, target_folder, source):
        file_name = os.path.basename(source)
        target = f'{target_folder}/{file_name}'
        urllib.request.urlretrieve(source, target)
        return target
