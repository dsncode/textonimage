from PIL import ImageFont
import textwrap
from app.model.payload import Split


class TextService:

    @staticmethod
    def __find_longest_word(text):
        longest_word = max(text.split(), key=lambda word: len(word))
        return longest_word, len(longest_word)

    @staticmethod
    def __find_combinations(text):
        response = []
        word, size = TextService.__find_longest_word(text)
        more = True
        width_ref = size
        while more:
            lines = textwrap.wrap(text, width=width_ref)
            if len(response) == 0 or (response[len(response) - 1] != lines):
                response.append(lines)

            width_ref = width_ref + 1
            if len(lines) == 1:
                more = False
        return response

    @staticmethod
    def find_best_combination(content, font_location, box):
        max_height = box.height
        max_width = box.width
        samples = TextService.__find_combinations(content)
        best_sample = None

        for sample in samples:
            font_size = 1
            more = True
            current_best_sample = None

            while more:
                font = ImageFont.truetype(font_location, font_size)
                longest_text = max(sample, key=len)
                text_size = font.getsize(longest_text)
                text_height = (font_size * len(sample))

                if text_size[0] >= max_width or text_height >= max_height:
                    score = 0

                    for line in sample:
                        text_size = font.getsize(line)
                        score = score + font_size * text_size[0] * text_size[1]

                    # this happens when no sample was found
                    if current_best_sample is None:
                        raise TextDoesNotFitBoxException()

                    current_best_sample['score'] = score

                    if best_sample is None:
                        best_sample = current_best_sample
                    elif best_sample['score'] <= current_best_sample['score']:
                        best_sample = current_best_sample

                    more = False

                else:
                    current_best_sample = {'text': sample, 'font_size': font_size}

                font_size = font_size + 1

        splits = []

        start_x = box.x
        start_y = box.y
        font_size = best_sample['font_size']

        for text_line in best_sample['text']:
            split = Split(content=text_line, font_size=font_size, x=start_x, y=start_y)
            splits.append(split)
            start_y = start_y + font_size

        return splits


class TextDoesNotFitBoxException(Exception):
    def __init__(self, message="Can not make a text fit in a box"):
        super().__init__(message)
