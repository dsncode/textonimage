"""
Controller responsible for manipulate the images
"""
from flask import Blueprint, request, abort, Response, send_file

from config.schema import drawSchema
from marshmallow import ValidationError
from app.services.images import ImagesService
from app.services.text import TextService, TextDoesNotFitBoxException
from app.model.payload import Text, Box, DrawResponse
from urllib import error
from app.handlers.response import create_response_from_object, create_response_from_message
import traceback
from http import HTTPStatus

bp_image = Blueprint(name=__name__, import_name=__name__)
image_service = ImagesService()


@bp_image.route("/draw", methods=["POST"])
def draw_text_box():
    response = None
    try:
        payload = request.get_json()
        drawSchema().load(payload)
        #print(bp_image.config["API_BASE_PATH"])
        # download image
        image_path = image_service.download_image(payload['image_url'])
        # download font
        font_path = image_service.download_font(payload['font_url'])

        box = Box.fromDict(payload['box'])
        text = Text.fromDict(payload['text'])
        splits = TextService.find_best_combination(text.content, font_path, box)

        patched_image_name = image_service.patch_image(image_path=image_path, font_path=font_path, box=box, splits=splits, text=text)
        response = DrawResponse(resource=patched_image_name, box=box, splits=splits)

    except ValidationError:
        abort(HTTPStatus.BAD_REQUEST)
    except error.HTTPError:
        return create_response_from_message("Can not get file's data from url", HTTPStatus.BAD_REQUEST)
    except TextDoesNotFitBoxException:
        return create_response_from_message("Can not make a text fit in a box", HTTPStatus.BAD_REQUEST)
    except Exception as err:
        traceback.print_exc()
        return create_response_from_message(str(err), HTTPStatus.INTERNAL_SERVER_ERROR)

    return create_response_from_object(response, HTTPStatus.CREATED)


@bp_image.route("/images/<filename>", methods=["GET"])
def retrieve_image(filename):
    """
    Blueprint method to get one image from the server
    """
    try:
        location = image_service.find_image_location(filename)
        return send_file(location, mimetype='image/jpeg')
    except FileNotFoundError:
        abort(404)
    except Exception as err:
        traceback.print_exc()
        return Response(status=500, response=str(err))
