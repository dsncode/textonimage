import json


class Text:
    def __init__(self, content="", text_color="#000000", border_color="#000000"):
        self.content = content
        self.text_color = text_color
        self.border_color = border_color

    @staticmethod
    def fromDict(entries):
        text = Text()
        text.__dict__.update(entries)
        return text


class Split:
    def __init__(self, content, font_size, x, y):
        self.content = content
        self.font_size = font_size
        self.x = x
        self.y = y


class Box:
    def __init__(self, height=100, width=100, x=0, y=0):
        self.height = height
        self.width = width
        self.x = x
        self.y = y

    @staticmethod
    def fromDict(entries):
        box = Box()
        box.__dict__.update(entries)
        return box


class DrawResponse:
    resource_base_url = "/api/v1/images"
    splits = []

    def __init__(self, resource, box, splits=[]):
        self.resource = f'{self.resource_base_url}/{resource}'
        self.box = box
        self.splits = splits



