from flask import Response, json


def create_response_from_object(obj, status):
    payload = json.dumps(obj, default=lambda o: o.__dict__, sort_keys=True, indent=4)
    return Response(status=status, response=payload, content_type="application/json")


def create_response_from_dic(payload, status):
    return Response(status=status, response=json.dumps(payload), content_type="application/json")


def create_response_from_message(message, status, e=""):
    response = {
        "code": status,
        "message": message
    }
    return create_response_from_dic(response, status)
