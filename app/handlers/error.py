from app.handlers.response import create_response_from_message


def create_error_handler(message, status):

    def create_response(e):
        return create_response_from_message(message, status, e)

    return create_response

