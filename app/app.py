"""
Module for managing Flask app's init functions
"""
import os
from app.handlers.error import create_error_handler
from flask import Flask
from config.config import ConfigName, get_config
from http import HTTPStatus
from app.controllers.index import bp_index
from app.controllers.image import bp_image


def create_app():
    """
    Method to init and set up the Flask application
    """
    flask_app = Flask(import_name="dipp_app")

    _init_config(flask_app)
    _register_blueprint(flask_app)
    _register_api_error(flask_app)

    return flask_app


def _init_config(app):
    """
    Method to initialize the configuration
    """
    env = os.getenv("FLASK_ENV", ConfigName.DEV.value)
    app.config.from_object(get_config(env))


def _register_blueprint(app):
    """
    Method to register the blueprint
    """
    prefix = app.config["API_BASE_PATH"]

    app.register_blueprint(bp_index, url_prefix=prefix)
    app.register_blueprint(bp_image, url_prefix=prefix)


def _register_api_error(app):
    """
    Method to register the api error
    """
    not_found = create_error_handler(status=HTTPStatus.NOT_FOUND, message="Nothing matches the given URI")
    app.register_error_handler(HTTPStatus.NOT_FOUND, not_found)

    method_does_not_exist = create_error_handler(status=HTTPStatus.METHOD_NOT_ALLOWED, message="Specified method is invalid for this resource")
    app.register_error_handler(HTTPStatus.METHOD_NOT_ALLOWED, method_does_not_exist)

    bad_request = create_error_handler(status=HTTPStatus.BAD_REQUEST, message="Please inspect your params or query valid")
    app.register_error_handler(HTTPStatus.BAD_REQUEST, bad_request)
